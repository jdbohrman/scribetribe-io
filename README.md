# Scribetribe.io

> Scribetribe is a fun project I'm playing around with using React and AWS Machine Learning infrastructure. .

The goal of this project is to leverage a React.js front end that takes audio file uploads from users and pushes them through AWS Transcribe with a Lambda API backend. 

![Screenshot](scribetribe.png)
`

## Meta

James D. Bohrman – [@jdbohrman](https://twitter.com/jdbohrman) – jdbohrman@outlook.com

[https://github.com/jdbohrman](https://github.com/jdbohrman/)

## Contributing

1. Fork it (<https://github.com/jdbohrman/scribetribe.io/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
